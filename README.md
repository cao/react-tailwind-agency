# 使用 React 和 TailwindCSS 构建企业官网

- [视频](https://www.bilibili.com/video/BV1Z44y1V7f4)

- [源码](https://github.com/zxuqian/code-examples/tree/master/react/react-tailwind-agency)

- 由 [Cruip](https://cruip.com/) 提供设计
